/**
* Retrieve and display weather data for Vancouver, BC from openweatermpa.
*/
const axios = require('axios');
const apiKey = process.env.openweathermap_APIkey;
const baseURL ='https://api.openweathermap.org/data/2.5/weather';
const city = 'Vancouver,BC';
const apiURL = `${baseURL}?q=${city}A&units=metric&appid=${apiKey}`;

async function get() {
  const response = await axios.get(apiURL);
  const data = response.data;
  return `${data.weather[0].description}, ${data.main.temp} C`;
};

module.exports = get;