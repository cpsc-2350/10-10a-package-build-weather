/**
* Retrieve and display weather data for Vancouver, BC from Environment Canada.
*/
const axios = require('axios');
const xml2js =  require ('xml2js');

const apiURL = 'https://weather.gc.ca/rss/city/bc-74_e.xml';

async function get() {
  const response = await axios.get(apiURL);
  const xml = response.data;
  const json = await xml2js.parseStringPromise(xml);
  return json.feed.entry[1].title[0];
};

module.exports = get;